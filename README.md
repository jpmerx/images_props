# Image properties editor

## Objectives

Update date and location of a pictures directories tree.

## Some resources

- [Exchangeable Image File Format - EXIF standard](https://www.cipa.jp/std/documents/e/DC-X008-Translation-2019-E.pdf)
- Python packages related to EXIF metadata management
    - [Pillow](https://pypi.org/project/Pillow/) Image management
    - [exif](https://exif.readthedocs.io/en/latest/usage.html) Image metadata management
    - [geopy](https://geopy.readthedocs.io/en/stable/) Locate the coordinates of addresses, cities, countries. *Based on
      [OpenStreetMap](https://www.openstreetmap.fr/). OpenStreetMap website can be useful to test how a location is
      recognized.*
